const mongoose = require("mongoose");

var blockedTokenSchema = mongoose.Schema({
  tokens: [String]
});

const BlockedToken = mongoose.model("blockedToken", blockedTokenSchema);

module.exports = BlockedToken;
