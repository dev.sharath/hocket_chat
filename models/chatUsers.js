const mongoose = require("mongoose");

var chatUserSchema = mongoose.Schema({
  profileName: {
    type: String
  }
  // profileImage: {
  //   type: String
  // },
  // status: {
  //   type: String,
  //   default: "hi, busy on work"
  // }
});

const ChatUser = (module.exports = mongoose.model("ChatUser", chatUserSchema));
