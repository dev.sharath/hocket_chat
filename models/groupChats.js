const mongoose = require("mongoose");

var GroupChatSchema = mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true,
    unique: true
  },
  admin: {
    type: String,
    required: true
  },
  users: [{ type: Object }],
  messages: [Object]
});

const GroupChat = (module.exports = mongoose.model(
  "GroupChat",
  GroupChatSchema
));
