/**
 * installing dependancies
 */
const router = require("express").Router();
const jwt = require("jsonwebtoken");
const crypto = require("../utils/crypto");
const cookieManage = require("../utils/cookieManage");
const tokenCheck = require("../utils/tokenCheck");
const dashboard = require("./chat");
const passport = require("passport");

/**
 * configuring things
 */
require("dotenv").config();
require("../config/passport")(passport);
router.use(passport.initialize());

/**
 * routers are here
 */
module.exports = function(User) {
  // router.use(crypto.decryption);
  router.post("/signup", function(req, res) {
    let newUser = new User({
      email: req.body.email,
      password: req.body.password
    });
    User.createUser(newUser, function(err, user) {
      if (!err && user) {
        res.json({ success: true, message: "User is registered" });
      } else {
        console.log(err);
        res.json({ success: false, message: "User is not registered" });
      }
    });
  });

  router.post("/login", function(req, res) {
    let { email, password } = req.body;
    User.getUserByEmail(email, function(err, user) {
      if (err) throw err;
      if (!user) {
        return res.json({
          success: false,
          message: "User or Password is not found"
        });
      }
      User.comparePassword(password, user.password, function(err, isMatch) {
        if (err) throw err;
        if (isMatch) {
          let token = jwt.sign(user.toJSON(), process.env.JWT_SECRET, {
            expiresIn: "86400000"
          });
          // res.header("Athorization", 'JWT ' + token)
          res.cookie("Authorization", token, { maxAge: 86400000 });
          res.json({ success: true, message: "logged in successfully" });
        } else {
          res.json({ success: false, message: "password does not match" });
        }
      });
    });
  });

  router.get(
    "/logout",
    tokenCheck.blockToken,
    cookieManage.clearCookieFromClient,
    function(req, res) {
      res.json({ success: true, message: "logged out successfully" });
    }
  );

  return router;
};
