/**
 * installing dependancies
 */
const router = require("express").Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const crypto = require("../utils/crypto");
const cookieManage = require("../utils/cookieManage");
const tokenCheck = require("../utils/tokenCheck");
const path = require("path");

/**
 * configuring things
 */
require("dotenv").config();
require("../config/passport")(passport);
router.use(passport.initialize());

/**
 * routers starts from here
 */
module.exports = function(User) {
  router.get(
    "/1",
    passport.authenticate("jwt", {
      session: false
    }),
    (req, res) => {
      res.sendFile(path.join(__dirname, "../client/index.html"));
    }
  );

  router.get(
    "/2",
    passport.authenticate("jwt", {
      session: false
    }),
    (req, res) => {
      res.sendFile(path.join(__dirname, "../client/index2.html"));
    }
  );

  return router;
};
