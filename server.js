"use strict";
/**
 * including npm modules
 */
const express = require("express"),
  app = express(),
  http = require("http").Server(app),
  io = require("socket.io")(http),
  mongoose = require("mongoose"),
  morgan = require("morgan"),
  cors = require("cors"),
  bcryptjs = require("bcryptjs"),
  bodyparser = require("body-parser"),
  User = require("./models/users"),
  path = require("path"),
  cookieParser = require("cookie-parser"),
  authentication = require("./routers/authentication"),
  socketChat = require("./services/chating"),
  dotenv = require("dotenv"),
  passport = require("passport"),
  chat = require("./routers/chat"),
  cookieManage = require("./utils/cookieManage"),
  jwt = require("jsonwebtoken"),
  GroupChats = require("./models/groupChats"),
  ChatUsers = require("./models/chatUsers");

dotenv.config();
app.use(cookieParser());
app.use("/static", express.static("./node_modules"));
app.use(express.static("./client"));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use(cors());
require("./config/passport")(passport);
app.use(passport.initialize());

/**
 * additional configuration
 */
mongoose
  .connect(process.env.DB_HOST, {
    useUnifiedTopology: true,
    useNewUrlParser: true
  })
  .then(status => {
    console.log("connected");
  })
  .catch(err => {
    console.log(err);
  });

/**
 * routers starts from here
 * =======================================================================================================================================
 */
app.get("/", function(req, res) {
  res.cookie("Authorization", process.env.Token);
  res.sendFile(path.join(__dirname, "./client/index.html"));
});
app.get("/login", (req, res) => {
  res.sendFile(__dirname + "/client/login.html");
});
app.get("/signup", (req, res) => {
  res.sendFile(__dirname + "/client/signup.html");
});
app.use("/authentication", authentication(User));
app.use("/chat", chat(User));
socketChat(io);

/**
 * app listening on port
 */
const port = process.env.PORT || 4000;
http.listen(port, () => {
  console.log(`API Gateway is running on port : ${port}`);
});
