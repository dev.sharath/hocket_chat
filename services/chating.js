/**
 * installing dependancies
 */
const passport = require("passport"),
  jwt = require("jsonwebtoken"),
  SocketIOFileUpload = require("socketio-file-upload"),
  GroupChats = require("../models/groupChats"),
  ChatUsers = require("../models/chatUsers"),
  cookieParser = require("cookie-parser"),
  cookieManage = require("../utils/cookieManage");

/**
 * additional configurations
 */
require("dotenv").config();

module.exports = function(io) {
  // socketChat(io);
  io.use(function(socket, next) {
    if (socket.handshake.headers.cookie) {
      let Auth = cookieManage.extractCookie(
        socket.handshake.headers.cookie,
        "Authorization"
      );
      jwt.verify(Auth, process.env.JWT_SECRET, function(err, decoded) {
        if (err) return next(new Error("Authentication error"));
        socket.decoded = decoded;
        next();
      });
    } else {
      next(new Error("Authentication error"));
    }
  });
  io.on("connection", function(socket) {
    console.log("connected " + socket.id);

    // check the the user is in our database or notify
    socket.on("initialize user", async user => {
      // check for user in our database
      let User = await ChatUsers.findOne({ profileName: user.name });
      if (User) {
        socket.join(user.name, () => {
          socket.emit("user initialized", { success: true, main: true });
        });
      } else {
        try {
          await new ChatUsers({
            profileName
          }).save();
          socket.join(user.name, () => {
            socket.emit("user initialized", { success: true });
          });
        } catch (err) {
          console.log(err);
          socket.emit("user initialized", { success: false });
        }
      }
    });

    // creating the new group
    socket.on("create new group", async (admin, group) => {
      try {
        let Group = await GroupChats.findOne({ name: group.name });
        if (!Group) {
          await new GroupChats({
            type: group.type,
            name: group.name,
            admin: admin.name
          }).save();
          socket.join(group.name, () => {
            // create a new group name in database and mension admin name
            socket.emit("success notification", {
              success: true,
              message: "group created successfully",
              admin,
              group
            });
          });
        } else {
          socket.emit("danger notification", {
            success: false,
            message: "group already existed"
          });
        }
      } catch (err) {
        console.log(err);
        socket.emit("danger notification", {
          success: false,
          message: "error creating group"
        });
      }
    });

    // adding member to the group
    socket.on("add new member", async (admin, group, newUser) => {
      try {
        let Group = await GroupChats.findOne({ name: group.name });
        if (Group.admin === admin.name) {
          let update = await GroupChats.updateOne(
            { _id: Group._id },
            {
              $push: {
                users: newUser
              }
            }
          );
          if (update) {
            socket.emit("success notification", {
              success: true,
              message: "member added successfully",
              admin,
              group,
              newUser
            });
          }
        }
      } catch (err) {
        console.log(err);
        socket.emit("danger notification", {
          success: false,
          message: "error adding to group"
        });
      }
    });

    // sending group chat to the perticular group
    socket.on("group chat send", async (user, group, chat) => {
      // upload chat to database
      try {
        let Group = await GroupChats.findOne({ name: group.name });
        if (Group.users.includes(user.name) || Group.admin === user.name) {
          socket.join(group.name);
          socket.to(group.name).emit("group chat receive", chat);
          if (Group) {
            await GroupChats.updateOne(
              { _id: Group._id },
              {
                $push: {
                  messages: chat
                }
              }
            );
          }
        }
      } catch (err) {
        console.log(err);
      }
    });

    // socket on connection ends here
  });
};

/**
 * socket.io configuration for chating app behaviour /** ************************************************************
 */
// module.exports = function(io) {
//   io.on("connection", socket => {
// console.log("connected", socket.id);
// db.online(socket.handshake.query.name, function(users) {
//   console.log(users);
//   io.emit("user added", users, socket.handshake.query.name, "Online");
//   // io.emit("group list", db.groups);
// });

/**
 ******************************************************* uploading files
 */
// var uploader = new SocketIOFileUpload();
// uploader.dir = __dirname + "/uploads";
// uploader.listen(socket);
// var currentFileId;

// uploader.on("start", function(event) {
//   console.log(event.file.name);
//   console.log(event.file.mtime);
//   console.log(event.file.encoding);
//   console.log(event.file.meta);
//   console.log(event.file.success);
//   console.log(event.file.bytesLoaded);
//   console.log(event.file.id);
//   currentFileId = event.file.id;
// });

// uploader.on("progress", function(event) {
//   // console.log(event.file)
//   // console.log(event.buffer)
// });

// socket.on("abord file uploading", msg => {
//   uploader.abort(currentFileId, socket);
// });

// uploader.on("complete", function(event) {
//   // console.log(event.file)
//   console.log(event.interrupt);
// });

// // Do something when a file is saved:
// uploader.on("saved", function(event) {
//   console.log("saving here ==");
//   console.log(event.file);
//   io.emit(
//     "receive file",
//     "http://localhost:8000/files/" + event.file.name,
//     event.file.name
//   );
//   console.log(event.file.bytesLoaded);
// });

// // Error handler:
// uploader.on("error", function(event) {
//   // console.log(event.file)
//   console.log("Error from uploader", event.error);
// });

/**************************************************************** */

// check the the user is in our database or notify
//     socket.on("initialize user", async user => {
//       // check for user in our database
//       let User = await ChatUsers.find({ profileName: user.name });
//       if (User) {
//         socket.join(user.name, () => {
//           socket.emit("user initialized", { success: true });
//         });
//       } else {
//         try {
//           await new ChatUsers({
//             profileName
//           }).save();
//           socket.join(user.name, () => {
//             socket.emit("user initialized", { success: true });
//           });
//         } catch (err) {
//           console.log(err);
//           socket.emit("user initialized", { success: false });
//         }
//       }
//     });

//     // creating the new group
//     socket.on("create new group", async (admin, group) => {
//       try {
//         let group = await GroupChats.find({ name: group });
//         if (!group) {
//           socket.join(group.name, () => {
//             // create a new group name in database and mension admin name
//             socket.emit("new group created", {
//               success: true,
//               message: "group created successfully"
//             });
//           });
//         } else {
//           socket.emit("new group created", {
//             success: false,
//             message: "group already existed"
//           });
//         }
//       } catch (err) {
//         socket.emit("new group created", {
//           success: false,
//           message: "error creating group"
//         });
//       }
//     });

//     // sending group chat to the perticular group
//     socket.on("group chat send", async (user, group, chat) => {
//       // upload chat to database
//       io.to(group.name).emit("group chat receive", user, chat, function(ack) {
//         if (ack === true) {
//           socket.emit("delevered", chat.id);
//         }
//       });
//       try {
//         let group = await GroupChats.find({ name: group });
//         if (group) {
//           let update = await GroupChats.updateOne(
//             { _id: group._id },
//             {
//               $push: {
//                 messages: { chat }
//               }
//             }
//           );
//         }
//       } catch (err) {
//         console.log(err);
//       }
//     });

//     // socket.io on connection ends here
//   });
//   // module exports ends here
// };

// *****************

// db.offline(socket.handshake.query.name, function(users) {
//   console.log(users);
//   io.emit("user added", users, socket.handshake.query.name, "Offline");
// });

// db.addGroup(group, function(groups) {
//   io.emit(
//     "notify",
//     person,
//     groups,
//     group,
//     " created one group name : " + group
//   );
//   console.log("group added : " + group);
// });

// Chat.getChat(id, function(err, chat) {
//   socket.emit("receive chats", chat.chats);
// });

// GroupChats.updateOne(
//   { _id: "5e1dc3981c9d4400003c78d1" },
//   {
//     $push: {
//       chats: {
//         from: data.from,
//         to: data.room,
//         message: data.msg,
//         date: "01/14/2020"
//       }
//     }
//   }
// ).exec((err, item) => {
//   if (err) {
//     console.log(err);
//     return socket.emit("send message error", err);
//   }
//   socket.broadcast
//     .to(data.room)
//     .emit("send message", `${data.from} : ${data.msg}`);
// });
