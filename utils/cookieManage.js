/**
 * installing dependancies
 */

/**
 * configuring object for functions
 */
let cookieManage = {};

cookieManage.clearCookieFromClient = function(req, res, next) {
  req.logout();
  res.clearCookie("Authorization");
  next();
};

cookieManage.extractCookie = function(rawCookie, cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(rawCookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};

module.exports = cookieManage;
