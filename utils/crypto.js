/**
 * installing dependancies
 */
let CryptoJS = require("crypto-js");

/**
 * configuring encryption and decryption func
 */
let crypto = {};

crypto.helper = function(string) {
  return CryptoJS.AES.decrypt(
    string,
    process.env.ENCRYPTION_SECRET_KEY
  ).toString(CryptoJS.enc.Utf8);
};

crypto.decryption = function(req, res, next) {
  let { email, password } = req.body;
  let decryptedEmail = this.helper(email),
    decryptedPassword = this.helper(password);
  console.log(decryptedEmail, decryptedPassword);
  (req.body.email = decryptedEmail), (req.body.email = decryptedPassword);
  next();
};

module.exports = crypto;
