/**
 * installing dependancies
 */
const BlockedToken = require("../models/blockedToken");

/**
 * configuring functions
 */
let tokens = {};

tokens.checkForBlockedToken = function(req, res, next) {
  if (req && req.cookies && req.cookies["Authorization"] != "") {
    var token = req.cookies["Authorization"];
  } else {
    return next();
  }
  BlockedToken.findById("5e170c5252ac8b3f4cf40f34", function(err, user) {
    if (err)
      return res.render("message", {
        success: false,
        message: "problem in server"
      });
    if (user && user.tokens.includes(token)) {
      return res.render("message", {
        success: false,
        message: "this token expired please login to get access"
      });
    } else {
      return next();
    }
  });
};

tokens.blockToken = function(req, res, next) {
  let token = req && req.cookies ? req.cookies["Authorization"] : false;
  if (token) {
    BlockedToken.updateOne(
      { _id: "5e170c5252ac8b3f4cf40f34" },
      { $push: { tokens: token } }
    ).exec((err, item) => {
      if (err)
        return res.render("message", {
          success: false,
          message: "Coud not able to log out"
        });
      return next();
    });
  } else {
    return next();
  }
};

module.exports = tokens;
